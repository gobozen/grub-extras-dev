
915resolution-0.5.3 -- http://915resolution.mango-lang.org/
- by Steve Tomljenovic, released 2007-04-17
- original command-line tool to patch vesa bios mode table, not maintained since 2007

grub-extras includes a modified 915resolution
based on patch for grub-v1.96  by Nathan Coulson at date?
- http://nathancoulson.com/proj_grub2_915.php
- http://nathancoulson.com/proj/eee/grub-1.96-915resolution-0.5.2-3.patch

grub-extras at grub-v2.02 includes:
- v0.5.3 + 945GME + 965GM, G33 + Q33, Q35 + 500GMA + GM45, GMA3150, HD3000
- tempestous' version - HDGraphics (0x00408086, 0x00448086) + HD3000 (0x01048086) + Q33 (0x29d08086)
- renamed: Q35 (t's G33: 0x29b08086) ; 965GM (t's GM965)
by Robert Millan at 2009-11-29 -- parent not found, maybe from grub git?
- http://git.savannah.gnu.org/cgit/grub-extras.git/commit/?id=638a7e5b5c3a219515cd6d07f4be6b7a8de11f69
by Vladimir 'phcoder' Serbinenko at 2012-09-08
- http://git.savannah.gnu.org/cgit/grub-extras.git/commit/?id=ae7f4b61873fb951c77cd01aa431a242b8d5348e
- added CT_GM45, CT_GMA3150, CT_HD3000


different patch thread by tempestous for Puppy Linux (last updated 2012-05-21)
- http://www.murga-linux.com/puppy/viewtopic.php?mode=attach&id=72709  --  915resolution-0.5.3-patched20120521.tar.gz
- http://www.murga-linux.com/puppy/viewtopic.php?t=32462  --  915resolution patched
- for "GM965" (at 2008-10-13), different from "965GM" patch (by David Krapohl at 2007-07-26)
- v0.5.3 + 500GMA + 945GME + GM965, G33, GM45, GMA3150, HDGraphics
- 500GMA: 0x81008086 ; 945GME: 0x27ac8086 ; GM965: 0x2a008086, 0x2a018086, 0x2a028086 ; G33: 0x29b08086, 0x29c08086 ; GM45: 0x2a408086
- GMA3150: 0xa0008086, 0xa0108086 ; HDGraphics: 0x00408086, 0x00448086
- UPDATE Sep 25 2008: G33 support now includes device id 29b0:8086.
- UPDATE Oct 13 2008: support added for GM965, device id 2a00:8086.
- UPDATE Dec 11 2009: ArchLinux patches added by Barry, then modified by me to support GM45 (GMA 4500MHD) device id 2a40:8086.
- UPDATE May 31 2010: support added for GMA3150 (N10 Pineview) device id a010:8086.
- UPDATE August 28 2010: support added for GMA X3100, device ID's 8086:2a01 and 8086:2a02. I have identified these two devices in the source code as part of the "GM965" chipset family.
- UPDATE Nov 23 2010: support added for a SECOND GMA3150 chipset, device id a000:8086
- UPDATE May 21 2012: support added for HDGraphics (Clarkdale/Arrandale) device id's 0040:8086 and 0044:8086
- Also code removed for HD3000/Sandy Bridge device ID's, because these are incompatible devices.


only "965GM" patch (by David Krapohl at 2007-07-26):
https://bugs.gentoo.org/show_bug.cgi?id=186661
https://186661.bugs.gentoo.org/attachment.cgi?id=126062

gentoo's 915resolution-0.5.3-r3.ebuild (2010-01-15, removed, includes "965GM" patch)
https://sources.gentoo.org/cgi-bin/viewvc.cgi/gentoo-x86/sys-apps/915resolution/?hideattic=0
https://github.com/p8952/915resolution-gentoo/tree/master/sys-apps/915resolution



Clover EFI has builtin automatic support to patch Vesa Bios video mode table with preferred resolution from monitor EDID
https://github.com/Clover-EFI-Bootloader/clover/blob/master/Library/VideoBiosPatchLib/VideoBiosPatchLib.c
https://clover-wiki.zetam.org/Configuration/Graphics#graphics_patchvbios
VideoBiosPatchNativeFromEdid
http://www.macbreaker.com/2012/06/how-to-set-your-hackintosh-bootscreen.html
http://www.insanelymac.com/forum/topic/211294-information-on-vesa-modes-in-atinvidia-bios/
